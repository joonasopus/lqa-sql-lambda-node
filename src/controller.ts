import * as mssql from 'mssql';

export const backup = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    //params = params[0];
    //console.log(params);
    //console.log('params', params);
    //return params;

    try {
        let pool = await mssql.connect(dbConfig);
        const source_db_name = params.source_db_name;
        const s3_arn_to_backup_to = params.s3_arn_to_backup_to;
        const overwrite_S3_backup_file = params.overwrite_S3_backup_file;
        return await pool.request()
            .input('source_db_name', mssql.VarChar(50), source_db_name)
            .input('s3_arn_to_backup_to', mssql.VarChar(50), s3_arn_to_backup_to)
            .input('overwrite_S3_backup_file', mssql.Int, overwrite_S3_backup_file)
            .execute('dbo.rds_backup_database');

    } catch (error) {
        console.log(error);
    }
    //console.log('do backup');
}

export const restore = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    try {
        let pool = await new mssql.ConnectionPool(dbConfig);
        const restore_db_name = 'event.restore_db_name';
        const s3_arn_to_restore_from = 'event.s3_arn_to_restore_from';
        return await pool.request()
            .input('restore_db_name', mssql.VarChar(2048), restore_db_name)
            .input('s3_arn_to_restore_from', mssql.VarChar(2048), s3_arn_to_restore_from)
            .execute('dbo.rds_restore_database');
    } catch (error) {
        console.log(error);
    }
    //console.log('do restore');
}

export const status = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    try {
        let pool = await new mssql.ConnectionPool(dbConfig);
        const db_name = params.db_name;
        const task_id = params.task_id;
        return await pool.request()
            .input('db_name', mssql.nvarchar(128), db_name)
            .input('task_id', mssql.nvarchar(128), task_id)
            .execute('dbo.rds_task_status');
    } catch (error) {
        console.log(error);
    }
    //console.log('do status');
}

export const list = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);
    //console.log(event);

    //console.log('dbname', event.restore_db_name);
    //console.log('restorefrom', event.restorefrom);

    try {
        dbConfig.database = 'master';
        let pool = await mssql.connect(dbConfig);
        let list = await pool.request().query('SELECT * FROM sys.databases WHERE name NOT IN (\'master\', \'model\', \'msdb\', \'tempdb\', \'rdsadmin\');');
        //const list = await pool.request().query('SELECT * FROM sys.databases');
        return list.recordset.map(item => item.name);
    } catch (error) {
        console.log(error);
    }
    //console.log('do list');
}

export const backupAll = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    params.forEach(async item => {
        try {
            return await backup(dbConfig, item);
        } catch (error) {
            console.log(error);
        }
    });

    //console.log('do backupAll');
}

export const dropAndRestore = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    params = params[0];

    try {
        let pool = await new mssql.ConnectionPool(dbConfig);
        const restore_db_name = 'event.restore_db_name';
        const s3_arn_to_restore_from = 'event.s3_arn_to_restore_from';
        let query = `IF EXISTS (SELECT name FROM sys.databases WHERE name = @${restore_db_name}) BEGIN; DECLARE @SQL NVARCHAR(MAX) = "ALTER DATABASE " + QUOTENAME(@${restore_db_name}) + " SET SINGLE_USER WITH ROLLBACK IMMEDIATE; DROP DATABASE " + QUOTENAME(@${restore_db_name}) + "; "; EXEC sp_executesql @SQL; END; exec msdb.dbo.rds_restore_database @${restore_db_name}=@${restore_db_name}, @${s3_arn_to_restore_from}=@${s3_arn_to_restore_from};`
        return await pool.request().query(query);
    } catch (error) {
        console.log(error);
    }
    //console.log('do dropAndRestore');
}

export const storedProcedure = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    try {
        let pool = await new mssql.ConnectionPool(dbConfig);
        const procedure = params.procedure;
        return await pool.request()
            .execute(procedure);
    } catch (error) {
        console.log(error);
    }
    //console.log('do dropAndRestore');
}

export const sql = async (dbConfig, params) => {
    if (dbConfig == null) throw new Error(`dbConfig missing: ${dbConfig}`);
    if (params == null) throw new Error(`params missing: ${params}`);

    try {
        let pool = await new mssql.ConnectionPool(dbConfig);
        const query = params.query;
        return await pool.request().query(query);
    } catch (error) {
        console.log(error);
    }
    //console.log('do dropAndRestore');
}