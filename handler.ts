import * as controller from './src/controller';
import { dbConfig } from './src/config/dbConfig';

module.exports.getAction = async (event, context, callback) => {
    const action = event.Action.toUpperCase();
    const params = event.ParametersList;
    console.log(action);

    if (action == null) throw new Error(`action missing: ${action}`);

    switch (action) {
        case 'BACKUP':
            return controller.backup(dbConfig, params);
            break;
        case 'RESTORE':
            return controller.restore(dbConfig, params);
            break;
        case 'STATUS':
            return controller.status(dbConfig, params);
            break;
        case 'LIST':
            return controller.list(dbConfig, params);
            break;
        case 'BACKUP_ALL':
            return controller.backupAll(dbConfig, params);
            break;
        case 'DROP_AND_RESTORE':
            return controller.dropAndRestore(dbConfig, params);
            break;
        case 'STORED_PROCEDURE':
            return controller.storedProcedure(dbConfig, params);
            break;
        case 'SQL':
            return controller.sql(dbConfig, params);
            break;
        default:
            return `Unknown action: ${action}`;
            break;
    }

}